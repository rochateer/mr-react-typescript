import React from 'react';
import logo from './logo.svg';
import './App.css';

let name: string;
let age: number;
let isStudent: boolean;
let hobbies: string[];
let role: [number, string];

let bothnumands : number | string; // "|" its called union 

//Bad
let printNameBad:Function; // Declare function

//Good
let printNameGood: (name:string) => void | never; // Declare function, after => its a return type
// void return undefined, never does not return anything

function printName(name : string){
  console.log(name)
}

printName("Ahmad")

//Bad
let personBad : Object;

//Good is using interface
interface Person {
  name : string;
  age? : number; // ? to make optional
}

interface Guy  extends Person{
  profession : string;
  level : number; // ? to make optional
}

type X = {
  a : string;
  b : string;
}
type Y = X & { //its will make Y using X property also
  c : string;
  d : string;
}

let lostOfPerson : Person[];

let person : Person = {
  name : "Ahmad",
  age : 25
}

let personNameUnk : unknown;

function AppBackup() {
  return (
    <div className="App">
      Hello World
    </div>
  );
}

export default AppBackup;
