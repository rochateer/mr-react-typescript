import React from 'react';
import { Todo } from '../model';
import { BsCheck, BsCheckAll, BsTrash } from 'react-icons/bs'

interface Props {
    todo : Todo;
    key : number;
    todos : Todo[];
    setTodos : React.Dispatch<React.SetStateAction<Todo[]>>;
}

const SingleTodo: React.FC<Props> = ({todo, key, setTodos, todos}) => {

  const completeTodo = (id:number) => {
    let todoList = [...todos];
    const todosId = todos.findIndex(t => t.id === id);
    todoList[todosId]["isDone"] = !todoList[todosId]["isDone"]
    setTodos(todoList)
  }

  const deleteTodo = (id:number) => {
    setTodos(todos.filter((t) => t.id !== id))
  }

  return (
    <form className='todos__single' key={key}>
        <span className="todos__single--text">
            {todo.todo}
        </span>
        <div>
          {todo.isDone ? (
            <span className="icon"><BsCheckAll /></span>
          ) : (
            <span className="icon" onClick={() => completeTodo(todo.id)}><BsCheck /></span>
          )}
          <span className="icon" onClick={() => deleteTodo(todo.id)}><BsTrash /></span>
        </div>
    </form>
  )
}

export default SingleTodo