import React, { useRef } from 'react';
import './styles.css';
import { Todo } from '../model';
import SingleTodo from './SingleTodo';

interface Props {
    todos : Todo[];
    setTodos : React.Dispatch<React.SetStateAction<Todo[]>>;
}

// const InputField = ({todo, setTodo}:Props) => { // you can use this
const TodoList: React.FC<Props> = ({todos, setTodos}) => {

  const inputRef = useRef<HTMLInputElement>(null)
  
  return (
    <div className="todos">
        {todos.map(todo => (
          <SingleTodo 
            todo={todo} 
            key={todo.id}
            setTodos={setTodos}
            todos={todos}
        />
        ))}
    </div>
  );
}

export default TodoList;